//
//  ViewController.swift
//  uipickerview
//
//  Created by Xiao Xiong Mao on 10/15/14.
//  Copyright (c) 2014 Aniyatech. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate {
    var cities = ["New York", "Orlando", "Las Vegas", "Seattle","Miami", "Milan","Beijing","La Paz","Paris","London"]
    
    @IBOutlet var cityLabel: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cities.count 
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
        return cities[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        cityLabel.text = cities[row]
    }
}

